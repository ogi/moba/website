# Open MOBA

MOBA stands for Multiplayer Online Battle Arena. It is a type of online strategy and action video game characterized by single games played individually or in teams against other players with one or more victory conditions per game.

Open MOBA is a free and open-source MOBA project. It aims to be a cooperative and competitive game.
